This an android app. Calling cloud Restful API hosted in Heroku.
 Android app communicates through sending XML request to server and receiving
 XML back in response. 
For converting the object to xml and  xml to object (Serializing, deserializing)
 "SimpleFramework" is used. "Simple Framework" is a light weight framework for 
marshaling and unmarshaling for android projects. There are some limitations for big 
XML data ( more than 100mb) but it works fine with medium size xml files.

[Presentation](https://docs.google.com/presentation/d/1ydwPw7JnVbBdjbzco925NeMhHwU8FoS8M7dUw7I0nAY/edit?usp=sharing)