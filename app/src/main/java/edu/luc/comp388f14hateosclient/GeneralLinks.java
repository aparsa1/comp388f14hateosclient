package edu.luc.comp388f14hateosclient;

import org.simpleframework.xml.ElementList;

import java.util.List;

import edu.luc.comp388f14hateosclient.Domain.Link;

/**
 * Created by aziz on 12/1/2014.
 */
public class GeneralLinks {


    public static  List< Link > links;

    public static String loginName;
    public static Link getLinkByAction(String action)
    {
        for(Link l : links)
        {
            if (l.getAction().equals(action))
                return l;
        }
        return null;
    }

}
