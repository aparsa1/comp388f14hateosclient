package edu.luc.comp388f14hateosclient;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import edu.luc.comp388f14hateosclient.Domain.BookRequest;
import edu.luc.comp388f14hateosclient.Domain.BooksRequestedRepresentation;


public class SearchActivity extends ActionBarActivity {

    public static BooksRequestedRepresentation booksRequestedRepresentation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SearchBook(View view) {
        String _url="",author,name,isbn;

        EditText txtAuthor=(EditText) findViewById(R.id.txtAuthor);
        author=txtAuthor.getText().toString();
        EditText txtIsbn=(EditText) findViewById(R.id.txtIsbn);
        isbn= txtIsbn.getText().toString();
        EditText txtName=(EditText)findViewById(R.id.txtName);
        name=txtName.getText().toString();
        BookRequest bookRequest =new BookRequest();

       if( !isbn.isEmpty())
           bookRequest.setIsbn(Integer.parseInt(isbn));
       if(!name.isEmpty())
        bookRequest.setName(name);
        if(!author.isEmpty())
        bookRequest.setAuthor(author);

        try{

         //   bookRequest.setAuthor("Tolkien");
            _url=GeneralLinks.getLinkByAction("SEARCH").getUrl();
            booksRequestedRepresentation =(BooksRequestedRepresentation)CallAPI.post(_url, BooksRequestedRepresentation.class, bookRequest);
           // Toast.makeText(this,xmlData.toString(),Toast.LENGTH_LONG).show();
                //booksRequestedRepresentation=(BooksRequestedRepresentation)CallAPI.post(_url, BooksRequestedRepresentation.class, bookRequest);

            Intent intent=new Intent(getApplicationContext(),BooksActivity.class);
            startActivity(intent);
            // Toast.makeText(getApplicationContext(),booksRequestedRepresentation.getBooks().size()+"search",Toast.LENGTH_LONG).show();
        }
        catch (Exception ex)
        {
            Toast.makeText(getApplicationContext(),"exception:"+ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void getAllBooks(View view) {
       // Links links=(Links) getIntent().getSerializableExtra("links");
        String _url="";
     //   for(links l : GeneralLinks.links.links)
       // {
         //   actions+= l.action;
       // }
        ///Toast.makeText(getApplicationContext(),actions,Toast.LENGTH_LONG).show();
        try{

         _url=GeneralLinks.getLinkByAction("GET_ALL_BOOKS").getUrl();
            if(!_url.isEmpty())
                booksRequestedRepresentation=(BooksRequestedRepresentation)CallAPI.get(_url,BooksRequestedRepresentation.class);

            Intent intent=new Intent(getApplicationContext(),BooksActivity.class);
            startActivity(intent);
       // Toast.makeText(getApplicationContext(),booksRequestedRepresentation.getBooks().size()+"search",Toast.LENGTH_LONG).show();
    }
    catch (Exception ex)
    {
        Toast.makeText(getApplicationContext(),"exception:"+ex.getMessage(), Toast.LENGTH_LONG).show();
    }
    }
}
