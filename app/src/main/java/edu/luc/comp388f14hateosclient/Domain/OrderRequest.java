package edu.luc.comp388f14hateosclient.Domain;

import org.simpleframework.xml.*;

import java.util.ArrayList;
import java.util.List;


@Root(name="OrderRequest")
public class OrderRequest
{
	// ----------------------------
	// Attributes
	// ----------------------------
    @Element(required = false)
	private int id;
    @Element(required = false)
	private Customer customer;
    @ElementList(entry="books",inline=true)
	public List< BookRepresentation > books;
    @Element(required = false)
	private Payment payment;
    @Element(required = false)
	private String date; // Pattern YYYY-MM-DD

    @Element(required = false)
    private Status status;

    @Element(required = false)
	private Address address;

	// ----------------------------
	// Constructor
	// ----------------------------

	public OrderRequest( )
	{
         books=new ArrayList<BookRepresentation>();
	}

	// ----------------------------
	// Methods
	// ----------------------------

	public void setId( int id )
	{
		this.id = id;
	}

	public int getId( )
	{
		return id;
	}

	public Customer getCustomer( )
	{
		return customer;
	}

	public void setCustomer( Customer customer )
	{
		this.customer = customer;
	}

	public Payment getPayment( )
	{
		return payment;
	}

	public void setPayment( Payment payment )
	{
		this.payment = payment;
	}

	public List< BookRepresentation > getBooks( )
	{
		return books;
	}

	public void setBooks( List< BookRepresentation > books )
	{
		this.books = books;
	}

	public String getDate( )
	{
		return date;
	}

	public void setDate( String date )
	{
		this.date = date;
	}

	public Status getStatus( )
	{
		return status;
	}

	public void setStatus( Status status )
	{
		this.status = status;
	}

	public Address getAddress( )
	{
		return address;
	}

	public void setAddress( Address address )
	{
		this.address = address;
	}

    public double getTotalCost()
    {
        double TotalCost=0.00;
        for(BookRepresentation b: books)
        {
            TotalCost+=b.getPrice();
        }
        return TotalCost;
    }
}