package edu.luc.comp388f14hateosclient.Domain;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;


@Root( name = "Order" )
public class OrderRepresentation
{
	// ----------------------------
	// Attributes
	// ----------------------------
    @ElementList(entry="links",inline=true)
    public List< Link > links;

    @Element(required=false,name="id")
	private Integer id;

    @Element(required=false,name="customer")
	private Customer customer;

    @ElementList(entry="books",inline=true)
	private List< BookRepresentation > books;

    @Element(required = false)
	private Address address;

    @Element(required=false)
	private Payment payment;

    @Attribute(name="date",required = false)
	private String date; // Pattern YYYY-MM-DD

    @Element(required =false)
	private Status status;


    public List< Link > getLinks( )
    {
        return links;
    }

    public Link getLinkByAction(String action)
    {
        for(Link l : links)
        {
            if (l.getAction().equals(action))
                return l;
        }
        return null;
    }
    public void setLinks( List< Link > links )
    {
        if ( null != links )
        {
            this.links = links;
        }
    }
	// ----------------------------
	// Constructor
	// ----------------------------

	public OrderRepresentation( )
	{

	}

	public OrderRepresentation( Customer customer, Payment payment, List< BookRepresentation > books, Address address, String date, Status status )
	{
		super( );
		this.customer = customer;
		this.payment = payment;
		this.books = books;
		this.setAddress( address );
		this.date = date;
		// this.status = status;
		// this.id = id;
	}

	// ----------------------------
	// Methods
	// ----------------------------

	public void setId( Integer id )
	{
		this.id = id;
	}

	public Integer getId( )
	{
		return id;
	}

	public Customer getCustomer( )
	{
		return customer;
	}

	public void setCustomer( Customer customer )
	{
		this.customer = customer;
	}

	public Payment getPayment( )
	{
		return payment;
	}

	public void setPayment( Payment payment )
	{
		this.payment = payment;
	}

	public List< BookRepresentation > getBooks( )
	{
		return books;
	}

	public void setBooks( List< BookRepresentation > books )
	{
		this.books = books;
	}

	public String getDate( )
	{
		return date;
	}

	public void setDate( String date )
	{
		this.date = date;
	}

	public Status getStatus( )
	{
		return status;
	}

	public void setStatus( Status status )
	{
		this.status = status;
	}

	public Address getAddress( )
	{
		return address;
	}

	public void setAddress( Address address )
	{
		this.address = address;
	}
}