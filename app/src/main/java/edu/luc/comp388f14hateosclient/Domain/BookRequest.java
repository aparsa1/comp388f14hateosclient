package edu.luc.comp388f14hateosclient.Domain;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="BookRequest")
public class BookRequest
{
	// ----------------------------
	// Attributes
	// ----------------------------
    @Element(required = false,name="author")
	private String author;

	@Element(required = false,name="name")
    private String name;

	@Element(required = false,name="isbn")
    private Integer isbn;

	// ----------------------------
	// Constructor
	// ----------------------------

	public BookRequest( )
	{
              author=null;
            name=null;
            isbn=null;
	}

	// ----------------------------
	// Methods
	// ----------------------------
	public Integer getIsbn( )
	{
		return isbn;
	}

	public void setIsbn( Integer isbn )
	{
		this.isbn = isbn;
	}

	public String getAuthor( )
	{
		return author;
	}

	public void setAuthor( String author )
	{
		this.author = author;
	}

	public String getName( )
	{
		return name;
	}

	public void setName( String name )
	{
		this.name = name;
	}

    @Override
    public String toString()
    {
        String result="";

        result="{\"author\": \""+author+"\", \"name\": "+name+", \"isbn\": "+isbn+" }";
        return result;

    }

}