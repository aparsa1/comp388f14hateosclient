package edu.luc.comp388f14hateosclient.Domain;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;


@Root( name = "Links" )
public class LinksRequestedRepresentation
{
    @ElementList(entry="links",inline=true,required = false)
    public List< Link > links;

    // ----------------------------
    // Methods
    // ----------------------------

    public List< Link > getLinks( )
    {
        return links;
    }

    public Link getLinkByAction(String action)
    {
        for(Link l : links)
        {
            if (l.getAction().equals(action))
                return l;
        }
        return null;
    }
    public void setLinks( List< Link > links )
    {
        if ( null != links )
        {
            this.links = links;
        }
    }
}