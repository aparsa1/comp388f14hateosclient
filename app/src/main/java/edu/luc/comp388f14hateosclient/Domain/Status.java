package edu.luc.comp388f14hateosclient.Domain;

import org.simpleframework.xml.Element;


@Element( name = "Status" )
public enum Status
{
	CONFIRMED, UNCONFIRMED, CANCELED, SHIPPED, DELIVERED, NONEXISTENT
}