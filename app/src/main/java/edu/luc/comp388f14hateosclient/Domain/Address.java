package edu.luc.comp388f14hateosclient.Domain;

import org.simpleframework.xml.Element;

public class Address
{
	// ----------------------------
	// Attributes
	// ----------------------------
    @Element(required = false)
	private String street;

    @Element(required = false)
	private String city;

    @Element(required = false)
	private String state;

    @Element(required = false)
	private Integer zipCode;

    @Element(required = false)
	private String country;

	// ----------------------------
	// Constructor
	// ----------------------------

	public Address( )
	{

	}

	public Address( String street, String city, String state, Integer zipCode, String country )
	{
		super( );
		this.street = street;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.country = country;
	}

	// ----------------------------
	// Methods
	// ----------------------------

	public String getStreet( )
	{
		return street;
	}

	public void setStreet( String street )
	{
		this.street = street;
	}

	public String getCity( )
	{
		return city;
	}

	public void setCity( String city )
	{
		this.city = city;
	}

	public String getState( )
	{
		return state;
	}

	public void setState( String state )
	{
		this.state = state;
	}

	public Integer getZipCode( )
	{
		return zipCode;
	}

	public void setZipCode( Integer zipCode )
	{
		this.zipCode = zipCode;
	}

	public String getCountry( )
	{
		return country;
	}

	public void setCountry( String country )
	{
		this.country = country;
	}

	public void setDummyData( )
	{
		this.street = "742 Evergreen Terrace";
		this.city = "Springfield";
		this.state = "MO";
		this.zipCode = 65801;
		this.country = "USA";
	}
}