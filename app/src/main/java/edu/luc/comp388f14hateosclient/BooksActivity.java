package edu.luc.comp388f14hateosclient;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.LayoutDirection;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.simpleframework.xml.Order;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import edu.luc.comp388f14hateosclient.Domain.*;


public class BooksActivity extends ActionBarActivity {
    OrderRequest orderRequest=new OrderRequest();
    public static OrderRepresentation orderRepresentation;
    BooksRequestedRepresentation booksRequestedRepresentation=SearchActivity.booksRequestedRepresentation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);


        if(booksRequestedRepresentation!=null)
        {
          TableRow tableRow;
            TableLayout tableLayout=(TableLayout) findViewById(R.id.tlMain);

            for(BookRepresentation b: booksRequestedRepresentation.getBooks())
            {
                tableRow=new TableRow(this);
                tableRow.setOrientation(LinearLayout.HORIZONTAL);
                TextView tvName=new TextView(this);
                tvName.setText(b.getName());
                TextView tvPrice=new TextView(this);
                tvPrice.setText("$"+String.format("%.2f", b.getPrice()));
                tvPrice.setTextColor(Color.GREEN);
                final BookRepresentation selectedBook=b;
                Button btnBuy=new Button(this);
                btnBuy.setText("Buy");
                btnBuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            orderRequest.books.add(selectedBook);
                            TextView tvBasket=(TextView) findViewById(R.id.tvBasket);
                            tvBasket.setText("Books in Basket: "+orderRequest.books.size());
                        }
                        catch (Exception e)
                        {
                            Logger log = Logger.getLogger("my.logger");
                            log.setLevel(Level.ALL);
                            ConsoleHandler handler = new ConsoleHandler();
                            handler.setLevel(Level.ALL);
                            handler.setFormatter(new SimpleFormatter());
                            log.addHandler(handler);
                            log.fine("Exception for "+selectedBook.getName()+e.getMessage());

                        }
                    }
                });

                tableRow.addView(btnBuy);
                tableRow.addView(tvPrice);
                 tableRow.addView(tvName);


                tableLayout.addView(tableRow);
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_books, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void PlaceOrder(View view) {
        try {
            Toast.makeText(this,"Placing Order...",Toast.LENGTH_SHORT).show();
            Customer customer = new Customer();
            customer.setDummyData();

            Payment payment = new Payment(orderRequest.getTotalCost());

            orderRequest.setCustomer(customer);
            orderRequest.setPayment(payment);

           orderRepresentation = (OrderRepresentation) CallAPI.post(booksRequestedRepresentation.getLinkByAction("BUY").getUrl(), OrderRepresentation.class, orderRequest);
            if(orderRepresentation==null)

                Toast.makeText(this,"Couldn't talk to server",Toast.LENGTH_LONG).show();
                else {
                Toast.makeText(this, "Order with ID " + orderRepresentation.getId().toString() + " " + orderRepresentation.getStatus().toString() + " .Total Cost: $" + orderRepresentation.getPayment().getCost(), Toast.LENGTH_LONG).show();
                Intent intent =new Intent(getApplicationContext(),OrderActivity.class);
                startActivity(intent);
            }
        }
        catch  (Exception e )
        {
            Toast.makeText(this,"Exception: "+e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    public void Search(View view) {
        Intent intent =new Intent(getApplicationContext(),SearchActivity.class);
        startActivity(intent);
    }
}
