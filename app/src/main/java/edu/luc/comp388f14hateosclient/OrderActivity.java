package edu.luc.comp388f14hateosclient;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.luc.comp388f14hateosclient.Domain.*;


public class OrderActivity extends ActionBarActivity {
  OrderRepresentation orderRepresentation=BooksActivity.orderRepresentation;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void Cancel(View view) {
        String _url="";
        Object result=new Object();
       try {
           if (orderRepresentation == null)
               Toast.makeText(this, "No order to cancel", Toast.LENGTH_LONG).show();
           else {
               _url = orderRepresentation.getLinkByAction("CANCEL").getUrl();
               if (_url.isEmpty() || _url == null) {
                   Toast.makeText(this, "No Link to cancel received", Toast.LENGTH_LONG).show();
               } else {
                 //  _url = _url.substring(0, _url.length() - 2);
                  // _url += orderRepresentation.getId();


                      result= CallAPI.delete(_url, OrderRepresentation.class);
                     orderRepresentation=(OrderRepresentation)result;
                     if(orderRepresentation==null)
                          Toast.makeText(this, "Couldn't connect to server", Toast.LENGTH_LONG).show();
                        else
                          Toast.makeText(this, "Status for this order is "+orderRepresentation.getStatus(), Toast.LENGTH_LONG).show();
                      FillTextBoxes();
               }
           }

       }
       catch (Exception e)
       {
           TextView txtAddress = (TextView) findViewById(R.id.txtAddress);
           txtAddress.setText("Exception: "+e.getMessage()+"\n"+"object: "+result.toString());

       }
    }

    public void getStatus(View view) {
        String _url="";
        Object result=new Object();
        try {

            if(orderRepresentation==null)
            Toast.makeText(this, "No order to get status", Toast.LENGTH_LONG).show();
        else
        {
            _url=orderRepresentation.getLinkByAction("GET_STATUS").getUrl();
            if(_url.isEmpty() || _url==null)
            {
                Toast.makeText(this, "No Link to get status received", Toast.LENGTH_LONG).show();
            }
            else
            {
               // _url=_url.substring(0,_url.length()-2);
             //   _url+=orderRepresentation.getId();

                result=CallAPI.get(_url,OrderRepresentation.class);
                orderRepresentation=(OrderRepresentation)result;

               if(orderRepresentation==null)
                   Toast.makeText(this, "Couldn't connect to server", Toast.LENGTH_LONG).show();
                else {
                   Toast.makeText(this, "Status for this order is " + orderRepresentation.getStatus(), Toast.LENGTH_LONG).show();
                  FillTextBoxes();
               }
            }
        }


    }
    catch (Exception e)
    {
        TextView txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtAddress.setText("Exception: "+e.getMessage()+"\n"+"object: "+result.toString());

    }
    }

    private void FillTextBoxes() {
        if(orderRepresentation!=null) {
            try {
                TextView txtOrderID = (TextView) findViewById(R.id.txtOrderID);
                TextView txtTotalCost = (TextView) findViewById(R.id.txtTotalPrice);
                TextView txtNumOfItems = (TextView) findViewById(R.id.txtNumOfItems);
                TextView txtAddress = (TextView) findViewById(R.id.txtAddress);
                TextView txtSatus = (TextView) findViewById(R.id.txtStatus);
                TextView txtClientName = (TextView) findViewById(R.id.txtClientName);
                TextView txtCardNum = (TextView) findViewById(R.id.txtCardNum);
                txtOrderID.setText("Order ID: " + orderRepresentation.getId().toString());
                txtTotalCost.setText("Total Price: " + orderRepresentation.getPayment().getCost().toString());
                txtNumOfItems.setText("Number of items: " + orderRepresentation.getBooks().size() + " items");
               txtSatus.setText("Status: " + orderRepresentation.getStatus().toString());
                txtClientName.setText("Shipped to:"+orderRepresentation.getCustomer().getName().toString());
                txtCardNum.setText("Using card num: ******"+orderRepresentation.getPayment().getCardNumber().toString().substring(0,4));
                txtAddress.setText("Shipped to: " + orderRepresentation.getAddress().getStreet() + " \n" + orderRepresentation.getAddress().getCity() +
                        "\n" + orderRepresentation.getAddress().getState() + " \n" + orderRepresentation.getAddress().getZipCode());
            }
            catch (Exception e)
            {}
        }

    }

    public void search(View view) {
        Intent intent =new Intent(getApplicationContext(),SearchActivity.class);
        startActivity(intent);
    }
}
