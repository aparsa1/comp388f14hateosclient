package edu.luc.comp388f14hateosclient;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.luc.comp388f14hateosclient.Domain.LinksRequestedRepresentation;


public class MainActivity extends ActionBarActivity {


 //private  final String url ="http://comp488f14bookstorehateoas.herokuapp.com/bookStoreService";

    private  final String url ="http://bookstorehateoas.herokuapp.com/bookStoreService";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }



    public void Start(View view) {

       try {


           EditText txtUserName=(EditText) findViewById(R.id.txtUserName);
           EditText txtPassword=(EditText) findViewById(R.id.txtPassword);
           String userName=txtUserName.getText().toString();
           String password=txtPassword.getText().toString();

           if(userName.isEmpty() || password.isEmpty())
           {
               TextView txtMessage=(TextView) findViewById(R.id.txtMessage);
               txtMessage.setText("Please enter your credentials!");
               return;
           }
           Toast.makeText(getApplicationContext(),"Authenticating...", Toast.LENGTH_SHORT).show();
            String Credentials="/?user="+userName+"&pass="+password;

           LinksRequestedRepresentation DynamicLinks= (LinksRequestedRepresentation)CallAPI.get(url+Credentials,LinksRequestedRepresentation.class);

           //Toast.makeText(getApplicationContext(),"object:"+DynamicLinks.links.get(0).getUrl(), Toast.LENGTH_LONG).show();
           if(DynamicLinks.links.size()==0) {
               TextView txtMessage=(TextView) findViewById(R.id.txtMessage);
               txtMessage.setText("Authentication Failed!");
               return;
           }
           GeneralLinks.links=DynamicLinks.getLinks();
           GeneralLinks.loginName=userName;
           Intent intent =new Intent(getApplicationContext(),SearchActivity.class);


            startActivity(intent);
       }
       catch (Exception ex)
       {
           TextView txtMessage=(TextView) findViewById(R.id.txtMessage);
           txtMessage.setText("Authentication Failed!");
         //Toast.makeText(getApplicationContext(),"exception: "+CallAPI.get(url,LinksRequestedRepresentation.class).toString(), Toast.LENGTH_LONG).show();
       }
    }
}
